# Documentation for the Welsh dictionary

## Returned results

### Adjectives

Returns the word, and if the word is comparable it will include
the same fields as the default adjective class. 
This is the String fields

* equative
* comparative
* superlative

### Nouns

### Verbs

All tenses comes as a JSON object with the following String fields:

* singFirst
* singSecond
* singThird
* plurFirst
* plurSecond
* plurThird
* impersonal

These might be empty, but should never be null.

Supported tenses:


| Tense       | Default  |
|-------------|----------|
| Present     | false    |
| Preterite   | true     |
| Imperfect   | false    |
| Future      | true     |
| Imperative  | true     |
| Conditional | true     |
| Pluperfect  | true     |

Note: Even though the ones labled default will usually be present, some words might not have all tenses. In that case the tense will not be present in the results.

### Mutations

All words will have the auto-generated field "mutations" unless they have the flagg "mutates" set to false in the dictionary. This is a JSON object
that contains the follow String fields

* init (unmutated form of the word)
* meddal (soft mutation)
* trywynol (nasal mutation)
* llaes (aspirate mutation)


