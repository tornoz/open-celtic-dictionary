# The Open Celtic Dictionary

Welcome the Open Celtic Dictionary project. The goal of the project is to be able to provide speakers of Celtic languages a dictionary API with all words, conjugations, some mutations, translations, and other useful information.

It is entirely open source and free for anyone to use.

## Notes

The dictionary doesn't contain all the words currently, but if given a word-type it will try to auto-generate the conjugations and other fields.

## Available dictionaries

* Breton (br)
* Welsh (cy)

## Usage

The API is hosted on http://prv.cymru:13999/api/dictionary, but can also be cloned and self hosted.
The API returns the result as UTF-8 formatted JSON.

### Parameters

| Parameter | Description                                                                                         | Default      |
|-----------|-----------------------------------------------------------------------------------------------------|--------------|
| word      | The word that you want to conjugate.                                                                | Required     |
| type      | The type of the word you want to conjugate. Current available types are: adjective, noun, and verb. |              |
| lang      | The language code of the dictionary you want to use.                                                | cy           |
| search    | A flag that sets whether to search conjugated words and other terms as well as the standard form.   | False        |

#### Results

If an error is accounted with the provided parameters, the API will return a JSON object with the success set to 'false'.
The field error will contain a detailed error message with the issue encountered.

If the API succeeds (meaning that it were able to handle the request, this does not mean it will return a result) the API will
return a JSON object with the success flag set to 'true' and an array called 'results' that contains all the words it found.

* For extended information about the returned results please refer to the documentation for each language.

### Examples

* Example with the Welsh word 'gweld' (to see): http://prv.cymru:13999/api/dictionary?word=gweld&math=verb
* Usage of the API in the bot Digi: https://gitlab.com/prvInSpace/digi

# Contributing

If you have found a word that is conjugated wrongly or you have any other enquiry or bug report, please feel free to either leave an issue on this page or send me an email.
I always need help with expanding the dictionary and to help me translate documentation into the different Celtic languages.

# Maintainers

* Preben Vangberg &lt;prv@aber.ac.uk&gt;

