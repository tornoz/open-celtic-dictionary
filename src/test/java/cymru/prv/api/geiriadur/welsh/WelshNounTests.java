package cymru.prv.api.geiriadur.welsh;

import cymru.prv.api.geiriadur.linguistics.common.WordType;
import cymru.prv.api.geiriadur.linguistics.welsh.WelshDictionary;
import org.json.JSONObject;
import org.junit.Test;

public class WelshNounTests {

    @Test
    public void testCeredigion(){
        JSONObject obj = new WelshDictionary().getWord("ceredigion", WordType.noun).getJSONObject(0);
        MutationTests.assertMutations(obj.getJSONObject("mutations"),
                "ceredigion", "geredigion", "ngheredigion", "cheredigion");
    }

}
