package cymru.prv.api.geiriadur;

import com.sun.net.httpserver.HttpServer;
import cymru.prv.api.geiriadur.httphandlers.GeneralHttpHandler;
import cymru.prv.api.geiriadur.httphandlers.InfoHttpHandler;
import cymru.prv.api.geiriadur.httphandlers.TestJsonHttpHandler;
import cymru.prv.api.geiriadur.httphandlers.WelshHttpHandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.util.concurrent.Executors;

public class APIServer {

    DictionaryList dictionaries;
    HttpServer server;

    private long connectionsHandled = 0;
    private boolean running = false;

    public DictionaryList getDictionaries() {
        return dictionaries;
    }

    private APIServer() throws Exception {
        System.out.println("Welcome to Geiriaduron Celtaidd Agor");
        dictionaries = new DictionaryList();
        server = HttpServer.create(new InetSocketAddress(13999), 0);

        server.createContext("/api/dictionary", new GeneralHttpHandler(this));
        server.createContext("/api/geiriadur", new WelshHttpHandler(this));
        server.createContext("/api/dictionary/info", new InfoHttpHandler(this));
        server.createContext("/api/dictionary/generate", new TestJsonHttpHandler(this));

        server.setExecutor(Executors.newFixedThreadPool(10));
        server.start();
        new Thread(this::commandlinePrompt).start();
    }

    private void commandlinePrompt(){
        running = true;
        BufferedReader reader = new BufferedReader( new InputStreamReader( System.in ) );
        while(running){
            System.out.print("\n> ");
            try {
                String s = reader.readLine();
                switch (s) {
                    case "reload":
                        try{
                            dictionaries = new DictionaryList();
                        }
                        catch (Exception e){
                            e.printStackTrace();
                        }
                        break;
                    case "stop":
                        running = false;
                        break;
                    case "status":
                        System.out.println("Connections handled: " + connectionsHandled);
                        System.out.println("Total words: " + dictionaries.getNumberOfWords());
                        System.out.println("Total RAM: " + (Runtime.getRuntime().totalMemory() / (1024L*1024L)) + " MB");
                        System.out.println("Used RAM:  " + ((Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / (1024L*1024L)) + " MB");
                        break;
                    default:
                        System.out.println("Unknown command");
                        break;
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        System.exit(0);
    }

    public static void main(String[] args) {
        try {
            new APIServer();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
