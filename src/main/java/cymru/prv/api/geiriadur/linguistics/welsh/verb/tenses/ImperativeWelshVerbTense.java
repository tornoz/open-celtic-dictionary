package cymru.prv.api.geiriadur.linguistics.welsh.verb.tenses;

import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ImperativeWelshVerbTense extends WelshVerbTense {

    public ImperativeWelshVerbTense(String stem, JSONObject obj) {
        super(stem, obj);
    }

    @Override
    protected List<String> defaultSingFirst() {
        return Collections.emptyList();
    }

    @Override
    protected List<String> defaultSingSecond() {
        return Arrays.asList(apply("a"));
    }

    @Override
    protected List<String> defaultSingThird() {
        return Collections.emptyList();
    }

    @Override
    protected List<String> defaultPlurFirst() {
        return Collections.emptyList();
    }

    @Override
    protected List<String> defaultPlurSecond() {
        return Arrays.asList(apply("wch"));
    }

    @Override
    protected List<String> defaultPlurThird() {
        return Collections.emptyList();
    }

    @Override
    public long numberOfConjugations() {
        return 2;
    }
}
