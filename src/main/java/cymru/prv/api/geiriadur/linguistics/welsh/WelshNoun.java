package cymru.prv.api.geiriadur.linguistics.welsh;

import cymru.prv.api.geiriadur.linguistics.common.*;
import org.json.JSONObject;

public class WelshNoun extends Noun implements Mutates {

    public WelshNoun(JSONObject obj) {
        super(obj);
    }

    public static WelshNoun fromJson(JSONObject obj) {
        return new WelshNoun(obj);
    }

    @Override
    public JSONObject getMutations() {
        return Mutation.getMutations(normalForm);
    }
}
