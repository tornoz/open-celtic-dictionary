package cymru.prv.api.geiriadur.linguistics.gaelic;

import java.util.Map;
import java.util.function.Function;

import cymru.prv.api.geiriadur.linguistics.common.InflectedPreposition;
import cymru.prv.api.geiriadur.linguistics.common.Noun;
import cymru.prv.api.geiriadur.linguistics.gaelic.verb.GaelicVerb;
import org.json.JSONObject;

import cymru.prv.api.geiriadur.AbstractDictionary;
import cymru.prv.api.geiriadur.DictionaryList;
import cymru.prv.api.geiriadur.linguistics.common.Word;
import cymru.prv.api.geiriadur.linguistics.common.WordType;
import cymru.prv.api.geiriadur.linguistics.irish.verb.IrishVerb;

public class GaelicDictionary extends AbstractDictionary {

    private static final String languageName = "Gaelic";
    private static final String languageCode = "gd";
    private static final Map<WordType, Function<JSONObject, Word>> wordTypeFromJsonMap = Map.of(
            WordType.verb, GaelicVerb::new,
            WordType.noun, Noun::new,
            WordType.preposition, InflectedPreposition::new
    );

    public GaelicDictionary(DictionaryList dictionaryList) {
        super(dictionaryList, languageName, languageCode, wordTypeFromJsonMap);
    }
}
