package cymru.prv.api.geiriadur.linguistics.common;

import cymru.prv.api.geiriadur.DictionaryList;
import cymru.prv.api.geiriadur.linguistics.welsh.Mutation;
import cymru.prv.api.geiriadur.translations.TranslationUnit;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public abstract class Word {

    public static long tempId = -1;

    private final long id;
    private WordType type;
    protected boolean onlyOnType;
    protected boolean mutates;
    protected String normalForm;
    protected String notes;
    protected String etymology;
    protected List<String> versions = new LinkedList<>();

    private final Map<String, String> IPAs = new HashMap<>();

    private List<Long> relatedIndexes;
    private List<Word> related;

    protected List<TranslationUnit> translations = new LinkedList<>();

    /**
     * Reads data from the JSON object
     * and fills out the necessary fields.
     *
     * @param obj a JSON object containing at least
     *            the field "normalForm". The field
     *            "notes" is optional
     */
    public Word(JSONObject obj, WordType type){
        id = obj.optLong("id", tempId--);
        normalForm = obj.getString("normalForm");
        notes = obj.optString("notes");
        etymology = obj.optString("etymology");
        mutates = obj.optBoolean("mutates", true);
        onlyOnType = obj.optBoolean("onlyOnType", false);
        if(obj.has("versions")){
            JSONArray versionArray = obj.getJSONArray("versions");
            for (int i = 0; i < versionArray.length(); i++)
                versions.add(versionArray.getString(i));
        }
        if(obj.has("ipa")){
            JSONArray array = obj.getJSONArray("ipa");
            for(int i = 0; i < array.length(); ++i){
                JSONObject ipa = array.getJSONObject(i);
                IPAs.put(ipa.getString("key"), ipa.getString("value"));
            }
        }
        if(obj.has("related")){
            relatedIndexes = new LinkedList<>();
            JSONArray array = obj.getJSONArray("related");
            for(int i = 0; i < array.length(); ++i)
                relatedIndexes.add(array.getLong(i));
        }
        this.type = type;
    }

    public void postLinker(DictionaryList list){
        if(relatedIndexes != null){
            related = new LinkedList<>();
            for(long index : relatedIndexes) {
                Word word = list.getWordAtIndex(index);
                if(word != null)
                    related.add(word);
                else
                    throw new IllegalStateException("Dictionary list does not contains related word!");
            }
        }
    }

    /**
     * Converts the word into a JSON object containing all the
     * conjugations, mutations, and other information that the
     * word may contain.
     *
     * @return the word as a JSONObject.
     */
    public abstract JSONObject toJson();

    /**
     * Creates a default JSONObject containing
     * all the standard information such as
     * the normal form and mutations.
     *
     * Notes is added if present.
     *
     * @return a new JSONObject with default fields.
     */
    protected JSONObject createBaseJson(){
        JSONObject obj = new JSONObject();
        obj.put("normalForm", normalForm);
        if(this instanceof Mutates)
            if(mutates)
                obj.put("mutations", ((Mutates)this).getMutations());
        obj.put("type", type);
        if(versions.size() != 0)
            obj.put("versions", new JSONArray(versions));
        if(notes != null && !notes.trim().isEmpty())
            obj.put("notes", notes);
        if(etymology != null && !etymology.trim().isEmpty())
            obj.put("etymology", etymology);

        JSONArray translationArray = new JSONArray();
        for(TranslationUnit unit : translations)
            translationArray.put(unit.toJson());
        obj.put("translations", translationArray);

        JSONArray ipas = new JSONArray();
        for(String key : IPAs.keySet())
            ipas.put(new JSONObject().put("key", key).put("value", IPAs.get(key)));

        if(related != null){
            JSONArray array = new JSONArray();
            for(Word w : related)
               array.put(new JSONObject().put("id", w.id).put("value", w.normalForm));
            obj.put("related", array);
        }

        return obj;
    }

    public String getNormalForm() {
        return normalForm;
    }

    public List<String> getVersions(){
        return versions;
    }

    public boolean isOnlyOnType() {
        return onlyOnType;
    }

    public long getNumberOfVersions(){
        JSONObject mutations = Mutation.getMutations(normalForm);
        int numberOfMutations = 0;
        if(mutates)
            numberOfMutations = (mutations.get(Mutation.SOFT).equals("") ? 0 : 1)
                    + (mutations.get(Mutation.NASAL).equals("") ? 0 : 1)
                    + (mutations.get(Mutation.ASPIRATE).equals("") ? 0 : 1);
        return 1 + numberOfMutations + versions.size();
    }

    public boolean matchesSearch(String search){
        return normalForm.equals(search)
                || versions.stream().anyMatch(s -> s.matches(search))
                || Mutation.getMutationsAsList(normalForm).stream().anyMatch(s -> s.matches(search));
    }

    public long getId() {
        return id;
    }

    public void addTranslation(TranslationUnit translation){
        translations.add(translation);
    }
}
