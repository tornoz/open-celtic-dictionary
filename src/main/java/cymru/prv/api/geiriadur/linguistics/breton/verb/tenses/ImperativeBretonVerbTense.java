package cymru.prv.api.geiriadur.linguistics.breton.verb.tenses;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ImperativeBretonVerbTense extends BretonVerbTense {

    public ImperativeBretonVerbTense(String stem, JSONObject obj) {
        super(stem, obj);
    }

    @Override
    protected List<String> defaultSingFirst() {
        return Collections.emptyList();
    }

    @Override
    protected List<String> defaultSingSecond() {
        return Arrays.asList(apply(""));
    }

    @Override
    protected List<String> defaultSingThird() {
        return Arrays.asList(apply("et"));
    }

    @Override
    protected List<String> defaultPlurFirst() {
        return Arrays.asList(apply("omp"));
    }

    @Override
    protected List<String> defaultPlurSecond() {
        return Arrays.asList(apply("it"));
    }

    @Override
    protected List<String> defaultPlurThird() {
        return Arrays.asList(apply("ent"));
    }

    @Override
    protected List<String> defaultImpersonal() {
        return Collections.emptyList();
    }
}
