package cymru.prv.api.geiriadur.linguistics.gaelic.verb.tenses;

import java.util.Collections;
import java.util.List;

import org.json.JSONObject;

import cymru.prv.api.geiriadur.linguistics.gaelic.GaelicLenition;

/**
 * 
 * @author Zander Urq. (zsharp68@gmail.com)
 * @since 06-03-2020 (MM-DD-YYYY)
 */
public class GaelicPastVerbTense extends GaelicVerbTense {

	public GaelicPastVerbTense(String stem, JSONObject obj) {
		super(stem, obj);
	}

	@Override
	protected List<String> defaultSingFirst() {
		return getDefaultOrFunction(this::defaultImpersonal);
	}

	@Override
	protected List<String> defaultSingSecond() {
		return getDefaultOrFunction(this::defaultImpersonal);
	}

	@Override
	protected List<String> defaultSingThird() {
		return getDefaultOrFunction(this::defaultImpersonal);
	}

	@Override
	protected List<String> defaultPlurFirst() {
		return getDefaultOrFunction(this::defaultImpersonal);
	}

	@Override
	protected List<String> defaultPlurSecond() {
		return getDefaultOrFunction(this::defaultImpersonal);
	}

	@Override
	protected List<String> defaultPlurThird() {
		return getDefaultOrFunction(this::defaultImpersonal);
	}

	@Override
	protected List<String> defaultImpersonal() {
		return Collections.singletonList(GaelicLenition.preformLenition(stem));
	}

}
