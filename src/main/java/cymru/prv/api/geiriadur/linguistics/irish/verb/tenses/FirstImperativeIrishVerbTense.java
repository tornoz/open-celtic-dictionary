package cymru.prv.api.geiriadur.linguistics.irish.verb.tenses;

import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

public class FirstImperativeIrishVerbTense extends IrishVerbTense {

    public FirstImperativeIrishVerbTense(String stem, JSONObject obj) {
        super(stem, obj);
    }

    @Override
    protected List<String> defaultAnalytic() {
        return Collections.emptyList();
    }

    @Override
    protected List<String> defaultSingFirst() {
        return Collections.emptyList();
    }

    @Override
    protected List<String> defaultSingSecond() {
        return Collections.singletonList(stem);
    }

    @Override
    protected List<String> defaultSingThird() {
        return Collections.emptyList();
    }

    @Override
    protected List<String> defaultPlurFirst() {
        return Collections.emptyList();
    }

    @Override
    protected List<String> defaultPlurSecond() {
        return Collections.singletonList(applyBroadOrSlender("aigí", "igí"));
    }

    @Override
    protected List<String> defaultPlurThird() {
        return Collections.emptyList();
    }

    @Override
    protected List<String> defaultImpersonal() {
        return Collections.emptyList();
    }
}
