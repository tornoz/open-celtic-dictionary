package cymru.prv.api.geiriadur.linguistics.welsh;

import java.util.regex.Pattern;

public class Syllable {

    private static final Pattern pattern = Pattern.compile("(ae|ai|au|aw|ei|eu|ew|iw|wy|yw|a|e|i|o|u|w|y)");

    public static long countSyllables(String word){
        return pattern.matcher(word).results().count();
    }

}
