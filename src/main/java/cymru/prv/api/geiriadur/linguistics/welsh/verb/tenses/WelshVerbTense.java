package cymru.prv.api.geiriadur.linguistics.welsh.verb.tenses;

import cymru.prv.api.geiriadur.linguistics.common.VerbTense;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public abstract class WelshVerbTense extends VerbTense {

    public WelshVerbTense(String stem, JSONObject obj) {
        super(stem, obj);
    }

    private static final Map<Character, String> makeLong = Map.of(
            'a', "â",
            'o', "ô",
            'e', "ee"
    );

    @Override
    protected List<String> defaultImpersonal() {
        return Collections.emptyList();
    }

    protected String apply(String suffix){
        if(stem.endsWith("" + suffix.charAt(0)) && makeLong.containsKey(suffix.charAt(0)) && suffix.length() > 1)
            return stem.replaceFirst( suffix.charAt(0) + "$", "" + makeLong.get(suffix.charAt(0))) + suffix.substring(1);
        if(stem.endsWith("" + suffix.charAt(0)))
                return stem + suffix.substring(1);
         return stem + suffix;
    }
}
