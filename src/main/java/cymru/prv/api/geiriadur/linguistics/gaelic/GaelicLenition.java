package cymru.prv.api.geiriadur.linguistics.gaelic;

import java.util.Arrays;
import java.util.Collections;

public class GaelicLenition {

    public static String preformLenition(String word){
        word = word
                .replaceFirst("^p", "ph")
                .replaceFirst("^t", "th")
                .replaceFirst("^c", "ch")
                .replaceFirst("^b", "bh")
                .replaceFirst("^d", "dh")
                .replaceFirst("^g", "gh")
                .replaceFirst("^m", "mh")
                .replaceFirst("^s", "sh")
                .replaceFirst("^f", "fh");
        if(word.matches("^(a|e|u|i|o|fh).*"))
            word = "dh'" + word;
        return word;
    }

    public static boolean isBroad(String word){
        int lastBroad = Collections.max(Arrays.asList(
                word.lastIndexOf("a"),
                word.lastIndexOf("u"),
                word.lastIndexOf("o")
        ));
        int lastSlender = Collections.max(Arrays.asList(
                word.lastIndexOf("e"),
                word.lastIndexOf("i")
        ));
        if(lastBroad > lastSlender)
            return true;
        return false;
    }

}
