package cymru.prv.api.geiriadur.linguistics.welsh;

import cymru.prv.api.geiriadur.linguistics.common.Adjective;
import cymru.prv.api.geiriadur.linguistics.common.Mutates;
import org.json.JSONObject;

public class WelshAdjective extends Adjective implements Mutates {

    private String stem;

    private static final int syllableThreshold = 2;

    private long syllables;

    public WelshAdjective(JSONObject obj){
        super(obj);
        syllables = Syllable.countSyllables(normalForm);
        stem = createStem(normalForm);
        stem = obj.optString("stem", stem);
    }


    protected String getEquative(){
        if(equative != null)
            return equative;
        if(syllables > syllableThreshold)
            return "mor " + Mutation.tregladMeddal(normalForm);
        return stem + "ed";
    }

    @Override
    protected String getComparative(){
        if(comparative != null)
            return comparative;
        if(syllables > syllableThreshold)
            return "mwy " + normalForm;
        return stem + "ach";
    }

    @Override
    protected String getSuperlative(){
        if(superlative != null)
            return superlative;
        if(syllables > syllableThreshold)
            return "mwya " + normalForm;
        return stem + "af";
    }


    private String createStem(String normalForm){
        if(syllables > 2)
            return null;
        if(normalForm.endsWith("b"))
            return normalForm.replaceFirst("b$", "p");
        if(normalForm.endsWith("g"))
            return normalForm.replaceFirst("g$", "c");
        return normalForm;
    }

    @Override
    public JSONObject getMutations() {
        return Mutation.getMutations(normalForm);
    }
}
