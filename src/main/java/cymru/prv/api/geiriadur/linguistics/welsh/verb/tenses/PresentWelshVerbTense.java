package cymru.prv.api.geiriadur.linguistics.welsh.verb.tenses;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

public class PresentWelshVerbTense extends WelshVerbTense {

    public PresentWelshVerbTense(String stem, JSONObject obj) {
        super(stem, obj);
    }

    @Override
    protected List<String> defaultSingFirst() {
        return Arrays.asList(stem);
    }

    @Override
    protected List<String> defaultSingSecond() {
        return Arrays.asList(stem);
    }

    @Override
    protected List<String> defaultSingThird() {
        return Arrays.asList(stem);
    }

    @Override
    protected List<String> defaultPlurFirst() {
        return Arrays.asList(stem);
    }

    @Override
    protected List<String> defaultPlurSecond() {
        return Arrays.asList(stem);
    }

    @Override
    protected List<String> defaultPlurThird() {
        return Arrays.asList(stem);
    }
}
