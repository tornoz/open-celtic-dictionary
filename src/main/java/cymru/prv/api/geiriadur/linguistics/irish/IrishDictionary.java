package cymru.prv.api.geiriadur.linguistics.irish;

import cymru.prv.api.geiriadur.AbstractDictionary;
import cymru.prv.api.geiriadur.DictionaryList;
import cymru.prv.api.geiriadur.linguistics.common.InflectedPreposition;
import cymru.prv.api.geiriadur.linguistics.common.Noun;
import cymru.prv.api.geiriadur.linguistics.common.Word;
import cymru.prv.api.geiriadur.linguistics.common.WordType;
import cymru.prv.api.geiriadur.linguistics.irish.verb.IrishVerb;
import org.json.JSONObject;

import java.util.Map;
import java.util.function.Function;

public class IrishDictionary extends AbstractDictionary {

    private static final String languageName = "Irish";
    private static final String languageCode = "ga";
    private static final Map<WordType, Function<JSONObject, Word>> wordTypeFromJsonMap = Map.of(
            WordType.verb, IrishVerb::new,
            WordType.noun, Noun::new,
            WordType.preposition, InflectedPreposition::new
    );

    public IrishDictionary(DictionaryList dictionaryList) {

        super(dictionaryList, languageName, languageCode, wordTypeFromJsonMap);
    }
}
