package cymru.prv.api.geiriadur.linguistics.common;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class InflectedPreposition extends Word {

    private boolean inflected;

    private Map<String, List<String>> inflections = new HashMap<>();

    private static final String[] inflectionTypes = new String[] {
            "singFirst",
            "singSecond",
            "singThird",
            "singThirdMasc",
            "singThirdFem",
            "plurFirst",
            "plurSecond",
            "plurThird",
    };
    
    /**
     * Reads data from the JSON object
     * and fills out the necessary fields.
     *
     * @param obj  a JSON object containing at least
     *             the field "normalForm". The field
     *             "notes" is optional
     */
    public InflectedPreposition(JSONObject obj) {
        super(obj, WordType.preposition);
        inflected = obj.optBoolean("inflected", false);
        if(inflected)
            for(String inflectionType : inflectionTypes)
                if(obj.has(inflectionType))
                    inflections.put(inflectionType, toStringList(obj.getJSONArray(inflectionType)));

    }

    @Override
    public JSONObject toJson() {
        JSONObject obj = createBaseJson();
        obj.put("inflected", inflected);
        if(inflected)
            for (String key : inflections.keySet())
                obj.put(key, inflections.get(key));
        return obj;
    }
    
    private List<String> toStringList(JSONArray array){
        List<String> strings = new LinkedList<>();
        for(int i = 0; i < array.length(); ++i)
            strings.add(array.getString(i));
        return strings;
    }
}
