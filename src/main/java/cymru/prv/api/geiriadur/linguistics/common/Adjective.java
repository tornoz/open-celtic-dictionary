package cymru.prv.api.geiriadur.linguistics.common;

import org.json.JSONObject;

public abstract class Adjective extends Word {

    protected String equative;
    protected String comparative;
    protected String superlative;

    protected boolean comparable;

    public Adjective(JSONObject obj){
        super(obj, WordType.adjective);
        comparable = obj.optBoolean("comparable", true);
        comparative = obj.optString("comparative", null);
        superlative = obj.optString("superlative", null);
        equative = obj.optString("equative", null);
    }

    @Override
    public JSONObject toJson() {
        JSONObject obj = createBaseJson();

        if(comparable) {
            String com = getComparative();
            if(com != null)
                obj.put("comparative", com);

            String sup = getSuperlative();
            if(sup != null)
                obj.put("superlative", sup);

            String equ = getEquative();
            if(equ != null)
                obj.put("equative", equ);
        }

        return obj;
    }

    protected abstract String getEquative();
    protected abstract String getComparative();
    protected abstract String getSuperlative();

    @Override
    public long getNumberOfVersions() {
        return super.getNumberOfVersions() + 3;
    }

    @Override
    public boolean matchesSearch(String search) {
        return super.matchesSearch(search)
                || getComparative().matches(search)
                || getSuperlative().matches(search)
                || getEquative().matches(search);
    }
}
