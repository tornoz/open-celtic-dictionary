package cymru.prv.api.geiriadur.linguistics.irish.verb.tenses;

import cymru.prv.api.geiriadur.linguistics.common.VerbTense;
import cymru.prv.api.geiriadur.linguistics.irish.IrishLenition;
import org.json.JSONObject;

import java.util.List;

public abstract class IrishVerbTense extends VerbTense {

    private List<String> analytic;

    protected abstract List<String> defaultAnalytic();

    protected List<String> getDefaultsOrAnalytic(){
        return getDefaultOrFunction(this::defaultAnalytic);
    }

    private List<String>getAnalytic(){
        return getValue(analytic, this::defaultAnalytic);
    }

    public IrishVerbTense(String stem, JSONObject obj) {
        super(stem, obj);
        analytic = getStringListOrNull(obj, "analytic");
    }

    @Override
    public JSONObject toJson() {
        return super.toJson().put("analytic", getAnalytic());
    }

    @Override
    protected String apply(String suffix) {
        if(stem.endsWith("í") && suffix.startsWith("i"))
            return stem + suffix.substring(1);
        if(stem.endsWith("á") && suffix.startsWith("a"))
            return stem + suffix.substring(1);
        if(stem.endsWith("" + suffix.charAt(0)))
            return stem + suffix.substring(1);
        return stem + suffix;
    }

    protected String applyBroadOrSlender(String broad, String slender) {
        if(IrishLenition.isBroad(stem))
            return apply(broad);
        else
            return apply(slender);
    }
}
