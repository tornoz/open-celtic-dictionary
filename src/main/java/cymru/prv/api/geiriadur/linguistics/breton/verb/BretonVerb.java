package cymru.prv.api.geiriadur.linguistics.breton.verb;

import cymru.prv.api.geiriadur.linguistics.breton.BretonMutation;
import cymru.prv.api.geiriadur.linguistics.breton.verb.tenses.*;
import cymru.prv.api.geiriadur.linguistics.common.Mutates;
import cymru.prv.api.geiriadur.linguistics.common.SpecialVerbTense;
import cymru.prv.api.geiriadur.linguistics.common.Verb;
import org.json.JSONObject;

public class BretonVerb extends Verb implements Mutates {
    /**
     * Reads data from the JSON object
     * and fills out the necessary fields.
     *
     * @param obj  a JSON object containing at least
     *             the field "normalForm". The field
     *             "notes" is optional
     */
    public BretonVerb(JSONObject obj) {
        super(obj);
    }

    @Override
    protected String getStem(String normalForm) {
        return normalForm.replaceFirst("(a|i|o|u|an|añ|at|ein|ed|eg|et|out|in|iñ|yd)$", "");
    }

    @Override
    protected void registerTenses(JSONObject obj) {
        addRequiredTense(obj, "present", PresentBretonVerbTense::new);
        addRequiredTense(obj, "imperfect", ImperfectBretonVerbTense::new);
        addRequiredTense(obj, "preterite", PreteriteBretonVerbTense::new);
        addRequiredTense(obj, "future", FutureBretonVerbTense::new);
        addRequiredTense(obj, "conditional_present", ConditionalPresentBretonVerbTense::new);
        addRequiredTense(obj, "conditional_imperfect", CondtitionalImperfectBretonVerbTense::new);
        addRequiredTense(obj, "imperative", ImperativeBretonVerbTense::new);

        addOptionalTense(obj, "present_habitual", PresentBretonVerbTense::new);
        addOptionalTense(obj, "present_situative", SpecialVerbTense::new);
        addOptionalTense(obj, "imperfect_habitual", ImperfectBretonVerbTense::new);
        addOptionalTense(obj, "imperfect_situative", SpecialVerbTense::new);
    }

    @Override
    public JSONObject getMutations() {
        return BretonMutation.getMutations(normalForm);
    }
}
