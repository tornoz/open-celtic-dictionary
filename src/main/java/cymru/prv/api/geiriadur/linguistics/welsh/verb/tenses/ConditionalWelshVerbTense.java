package cymru.prv.api.geiriadur.linguistics.welsh.verb.tenses;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

public class ConditionalWelshVerbTense extends WelshVerbTense {

    public ConditionalWelshVerbTense(String stem, JSONObject obj) {
        super(stem, obj);
    }

    @Override
    protected List<String> defaultSingFirst() {
        if(stem == null)
            System.out.println("");
        return Arrays.asList(apply("wn"));
    }

    @Override
    protected List<String> defaultSingSecond() {
        return Arrays.asList(apply("et"));
    }

    @Override
    protected List<String> defaultSingThird() {
        return Arrays.asList(apply("ai"));
    }

    @Override
    protected List<String> defaultPlurFirst() {
        return Arrays.asList(apply("en"));
    }

    @Override
    protected List<String> defaultPlurSecond() {
        return Arrays.asList(apply("ech"));
    }

    @Override
    protected List<String> defaultPlurThird() {
        return Arrays.asList(apply("en"));
    }
}
