package cymru.prv.api.geiriadur.linguistics.breton.verb.tenses;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

public class FutureBretonVerbTense extends BretonVerbTense {

    public FutureBretonVerbTense(String stem, JSONObject obj) {
        super(stem, obj);
    }

    @Override
    protected List<String> defaultSingFirst() {
        return Arrays.asList(apply("in"));
    }

    @Override
    protected List<String> defaultSingSecond() {
        return Arrays.asList(apply("i"));
    }

    @Override
    protected List<String> defaultSingThird() {
        return Arrays.asList(apply("o"));
    }

    @Override
    protected List<String> defaultPlurFirst() {
        return Arrays.asList(apply("imp"));
    }

    @Override
    protected List<String> defaultPlurSecond() {
        return Arrays.asList(apply("ot"));
    }

    @Override
    protected List<String> defaultPlurThird() {
        return Arrays.asList(apply("int"));
    }

    @Override
    protected List<String> defaultImpersonal() {
        return Arrays.asList(apply("or"));
    }
}
