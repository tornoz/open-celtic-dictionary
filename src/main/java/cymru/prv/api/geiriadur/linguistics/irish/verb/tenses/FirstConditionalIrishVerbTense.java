package cymru.prv.api.geiriadur.linguistics.irish.verb.tenses;

import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

public class FirstConditionalIrishVerbTense extends LenitedIrishVerbTense {

    public FirstConditionalIrishVerbTense(String stem, JSONObject obj) {
        super(stem, obj);
    }

    @Override
    protected List<String> defaultAnalytic() {
        return Collections.singletonList(applyBroadOrSlender("fadh", "feadh"));
    }

    @Override
    protected List<String> defaultSingFirst() {
        return Collections.singletonList(applyBroadOrSlender("fainn", "finn"));
    }

    @Override
    protected List<String> defaultSingSecond() {
        return Collections.singletonList(applyBroadOrSlender("fá", "feá"));
    }

    @Override
    protected List<String> defaultSingThird() {
        return getDefaultsOrAnalytic();
    }

    @Override
    protected List<String> defaultPlurFirst() {
        return Collections.singletonList(applyBroadOrSlender("faimis", "fimis"));
    }

    @Override
    protected List<String> defaultPlurSecond() {
        return getDefaultsOrAnalytic();
    }

    @Override
    protected List<String> defaultPlurThird() {
        return Collections.singletonList(applyBroadOrSlender("faidís", "fidís"));
    }

    @Override
    protected List<String> defaultImpersonal() {
        return Collections.singletonList(applyBroadOrSlender("faí", "fí"));
    }
}
