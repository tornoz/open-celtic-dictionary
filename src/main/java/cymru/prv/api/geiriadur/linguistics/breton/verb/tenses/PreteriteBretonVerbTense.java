package cymru.prv.api.geiriadur.linguistics.breton.verb.tenses;

import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

public class PreteriteBretonVerbTense extends BretonVerbTense {

    public PreteriteBretonVerbTense(String stem, JSONObject obj) {
        super(stem, obj);
    }

    @Override
    protected List<String> defaultSingFirst() {
        return Collections.singletonList(apply("is"));
    }

    @Override
    protected List<String> defaultSingSecond() {
        return Collections.singletonList(apply("jout"));
    }

    @Override
    protected List<String> defaultSingThird() {
        return Collections.singletonList(apply("as"));
    }

    @Override
    protected List<String> defaultPlurFirst() {
        return Collections.singletonList(apply("jomp"));
    }

    @Override
    protected List<String> defaultPlurSecond() {
        return Collections.singletonList(apply("joc'h"));
    }

    @Override
    protected List<String> defaultPlurThird() {
        return Collections.singletonList(apply("jont"));
    }

    @Override
    protected List<String> defaultImpersonal() {
        return Collections.singletonList(apply("jod"));
    }
}
