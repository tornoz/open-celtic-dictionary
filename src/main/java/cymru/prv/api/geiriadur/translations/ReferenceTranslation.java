package cymru.prv.api.geiriadur.translations;

import cymru.prv.api.geiriadur.linguistics.common.Word;
import org.json.JSONObject;

public class ReferenceTranslation implements ITranslation{

    private final Word word;

    public ReferenceTranslation(Word word) {
        this.word = word;
    }

    @Override
    public JSONObject getTranslation() {
        return new JSONObject().put("id", word.getId()).put("value", word.getNormalForm());
    }
}
