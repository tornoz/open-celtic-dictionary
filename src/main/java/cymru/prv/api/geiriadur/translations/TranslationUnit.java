package cymru.prv.api.geiriadur.translations;

import cymru.prv.api.geiriadur.DictionaryList;
import cymru.prv.api.geiriadur.linguistics.common.Word;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class TranslationUnit {

    Map<String, List<ITranslation>> translationMap = new HashMap<>();

    public TranslationUnit(DictionaryList list, JSONObject obj){
        for(String languageCode : obj.keySet()){
            List<ITranslation> translationsList = new LinkedList<>();
            translationMap.put(languageCode, translationsList);

            JSONArray translations = obj.getJSONArray(languageCode);
            for (int i = 0; i < translations.length(); i++) {
                Object translation = translations.get(i);
                if(translation instanceof String){
                    translationsList.add(new LiteralTranslation((String) translation));
                }
                else {
                    long index = translations.getLong(i);
                    Word word = list.getWordAtIndex(index);
                    if(word != null) {
                        translationsList.add(new ReferenceTranslation(word));
                        word.addTranslation(this);
                    }
                }
            }
        }
    }

    public JSONObject toJson(){
        JSONObject obj = new JSONObject();
        for(String langCode : translationMap.keySet()){
            JSONArray array = new JSONArray();
            for(ITranslation trans : translationMap.get(langCode))
                array.put(trans.getTranslation());
            obj.put(langCode, array);
        }
        return obj;
    }
}
