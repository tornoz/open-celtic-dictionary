package cymru.prv.api.geiriadur.translations;

import org.json.JSONObject;

public interface ITranslation {
    JSONObject getTranslation();
}
