package cymru.prv.api.geiriadur.httphandlers;

import com.sun.net.httpserver.HttpExchange;
import cymru.prv.api.geiriadur.APIServer;
import cymru.prv.api.geiriadur.linguistics.common.WordType;

import java.util.Map;

public class GeneralHttpHandler extends AbstractHttpHandler {

    public GeneralHttpHandler(APIServer server) {
        super(server);
    }

    @Override
    protected void handleRequest(HttpExchange exchange, Map<String, String> getVariables) {

        // Get word
        if(!getVariables.containsKey("word")) {
            sendBadRequest(exchange, "Missing field 'word'");
            return;
        }
        String word = getVariables.get("word");

        // Get type
        WordType type = null;
        if(getVariables.containsKey("type")){
            try{
                type = WordType.valueOf(getVariables.get("type"));
            }
            catch (Exception e){
                sendBadRequest(exchange, "Unknown type '" + getVariables.get("type") + "'");
                return;
            }
        }

        // Get type
        boolean search = false;
        if(getVariables.containsKey("search")){
            try{
                search = Boolean.parseBoolean(getVariables.get("search"));
            }
            catch (Exception e){
                sendBadRequest(exchange, "");
                return;
            }
        }

        // Get
        String languageCode = "cy";
        if(getVariables.containsKey("lang"))
            languageCode = getVariables.get("lang");

        // Handle
        dictionaryQuery(exchange, languageCode, word, search, type);
    }
}
