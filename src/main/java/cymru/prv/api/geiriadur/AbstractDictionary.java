package cymru.prv.api.geiriadur;

import cymru.prv.api.geiriadur.linguistics.common.Word;
import cymru.prv.api.geiriadur.linguistics.common.WordType;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

/**
 * The main dictionary class. This represent a normal dictionary.
 *
 * The class contains a map of dictionaries containing exceptions
 * from the standard conjunction and stem rules.
 *
 * It expects to find the different JSON exception files in the relative folder res/
 *
 * @author Preben Vangberg
 * @since 2020-04-21
 */
public abstract class AbstractDictionary {

    private final DictionaryList dictionaryList;

    private Map<WordType, Map<String, List<Word>>> dictionaryMap = new HashMap<>();
    private final Map<WordType, Function<JSONObject, Word>> wordTypeFromJsonMap;

    private final String languageCode;
    private final String languageName;

    private long numberOfWords = 0;

    public AbstractDictionary(DictionaryList dictionaryList, String languageName, String languageCode, Map<WordType, Function<JSONObject, Word>> wordTypeToJsonMap) {
        this.dictionaryList = dictionaryList;
        this.languageName = languageName;
        this.languageCode = languageCode;
        this.wordTypeFromJsonMap = wordTypeToJsonMap;
        System.out.println("\nLoading dictionary: " + languageCode);
        try {
            loadDictionaries();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Generates the path of an exception file based on the language code and the
     * word type.
     *
     * @param type The word-type of the exception file.
     * @return The path to the exception file as a String.
     */
    private String getPathForListOfWordsForType(WordType type){
        return languageCode + "/" + type.toString() + "s.json";
    }


    /**
     * Tries to load all the exception dictionary files from the res/"language-code"/ folder.
     * If any of them fail it throws an IOException.
     *
     * @throws IOException if one of the files couldn't be read.
     */
    public void loadDictionaries() throws IOException {
        numberOfWords = 0;

        for (WordType type : WordType.values()){
            if(wordTypeFromJsonMap.containsKey(type))
                dictionaryMap.put(type, buildDictionary(type, getPathForListOfWordsForType(type)));
            else
                dictionaryMap.put(type, new HashMap<>());
        }

        long total = 0;
        for (WordType type: dictionaryMap.keySet()) {
            int size = dictionaryMap.get(type).size();
            total += size;
            System.out.println("Loaded " + size + " " + type + "s");

        }
        System.out.println("Total base words: " + total);
        System.out.println("Amount of words: " + numberOfWords);
    }


    /**
     * Reads the exception file for the given word type and language.
     * Uses the wordtype to JSON map to populate the word.
     * Also adds the word to the dictionary list object if it exist.
     *
     * @param type Word type of the exception list
     * @param path The path of the file
     * @return A map containing the normalForm and versions of words as keys and their associated
     *          word object as value
     * @throws IOException If the file could not be read
     * @throws IllegalStateException
     */
    private Map<String, List<Word>> buildDictionary(WordType type, String path) throws IOException, IllegalStateException {
        JSONArray array = new JSONArray(Files.readString(Path.of("res/" + path)));
        Map<String, List<Word>> dictionary = new HashMap<>();
        for (int i = 0; i < array.length(); i++) {
            JSONObject object = array.getJSONObject(i);
            String normalForm = object.getString("normalForm");
            Word word = wordTypeFromJsonMap.get(type).apply(object);
            dictionary.putIfAbsent(normalForm, new LinkedList<>());
            List<Word> list = dictionary.get(normalForm);
            list.add(word);
            if(dictionaryList != null)
                dictionaryList.putWord(word);
            numberOfWords += word.getNumberOfVersions();
            for(String version : word.getVersions()) {
                dictionary.putIfAbsent(version, new LinkedList<>());
                dictionary.get(version).add(word);
            }
        }
        return dictionary;
    }


    /**
     * Tries to get a word from the exception dictionary. Otherwise it generates a new
     * word with the default conjugations.
     *
     * @param word The word you want to conjugate.
     * @param type The type of the word you want to conjugate.
     * @return A JSONArray containing conjugations, stems, mutations, and other information about the words.
     */
    public JSONArray getWord(String word, WordType type) {
        List<Word> words = null;
        Map<String, List<Word>> dictionary = dictionaryMap.get(type);

        boolean confirmed = true;

        if(dictionary != null)
            words = dictionary.get(word);

        if (words == null) {
            confirmed = false;
            JSONObject temp = new JSONObject();
            temp.put("normalForm", word);
            words = new LinkedList<>();
            words.add(wordTypeFromJsonMap.get(type).apply(temp));
        }

        return wordListToJsonArray(words, confirmed);
    }


    /**
     * Fetches the words from the dictionary with the given search term regardless of type.
     * The word needs to be know for this to work.
     *
     * @param word The desired word.
     * @return Returns a list of found words as a JSONArray
     */
    public JSONArray getWord(String word){
        List<Word> words = new LinkedList<>();
        for (Map<String, List<Word>> dictionary: dictionaryMap.values()) {
            if(dictionary.containsKey(word)){
                for(Word w : dictionary.get(word))
                    if(!w.isOnlyOnType())
                       words.add(w);
            }
        }

        JSONArray array = new JSONArray();
        for (Word w: words)
            array.put(w.toJson().put("confirmed", true));

        return array;
    }


    /**
     * Searches for a word in the dictionary. This includes the conjugated forms
     * as well as other fields as defined by the inherited function matchesSearch in the
     * abstract class Word.
     *
     * The word needs to be known for this to work.
     *
     * @param word The search term
     * @param max The max number of results
     * @return The results as a JSONArray.
     */
    public JSONArray searchForWord(String word, int max){
        List<Word> results = new LinkedList<>();
        for(Map<String, List<Word>> dict : dictionaryMap.values()) {
            for (List<Word> words : dict.values()) {
                for (Word w : words) {
                    if (w.matchesSearch(word) && !results.contains(w)) {
                        results.add(w);
                        if (results.size() >= max)
                            return confirmedWordListToJsonArray(results);
                    }
                }
            }
        }
        return confirmedWordListToJsonArray(results);
    }


    /**
     * Uses the given JSONObject to generate a word of the given type
     */
    public JSONArray generateWord(JSONObject obj, WordType type){
       return new JSONArray().put(wordTypeFromJsonMap.get(type).apply(obj).toJson().put("confirmed", false));
    }


    /**
     * Helper function that converts a list of words to a JSONArray.
     *
     * All the words are given the parameter confirmed as the confirmed-flag
     *
     * @param words The list of words
     * @param confirmed The confirmed status to give the words.
     * @return The list of words as a JSONArray
     */
    public static JSONArray wordListToJsonArray(List<Word> words, boolean confirmed){
        JSONArray array = new JSONArray();
        for (Word w: words)
            array.put(w.toJson().put("confirmed", confirmed));
        return array;
    }


    /**
     * Helper function that calls the wordListToJsonArray function with the confirmed-flag
     * true
     *
     * @param words The list of words
     * @return The list of words as a JSONArray
     */
    private JSONArray confirmedWordListToJsonArray(List<Word> words){
        return wordListToJsonArray(words, true);
    }


    /**
     * Fetches the language code of the dictionary
     *
     * @return The language code of the dictionary
     */
    public String getLanguageCode() {
        return languageCode;
    }


    /**
     * Fetches the language name of the dictionary
     *
     * @return The language name of the dictionary
     */
    public String getLanguageName() {
        return languageName;
    }

    /**
     * Returns the number of searchable words and conjugations.
     *
     * @return Returns the number of searchable words
     */
    public long getNumberOfWords() {
        return numberOfWords;
    }
}
